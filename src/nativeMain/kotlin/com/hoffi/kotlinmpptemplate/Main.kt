package com.hoffi.kotlinmpptemplate

fun main() {
    println("==================================================")
    println("===   Hello, Kotlin/Native! (${Platform.osFamily} / ${Platform.cpuArchitecture})   ===")
    println("==================================================")
}

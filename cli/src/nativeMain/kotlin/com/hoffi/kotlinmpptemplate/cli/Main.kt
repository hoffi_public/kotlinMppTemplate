package com.hoffi.kotlinmpptemplate.cli

fun main() {
    println("============================================")
    println("===   Hello, CLI Kotlin/Native! (${Platform.osFamily} / ${Platform.cpuArchitecture})   ===")
    println("============================================")
}

package com.hoffi.kotlinmpptemplate.shared

fun main() {
    println("==================================================")
    println("===   Hello, shared Kotlin/Native! (${Platform.osFamily} / ${Platform.cpuArchitecture})   ===")
    println("==================================================")
}

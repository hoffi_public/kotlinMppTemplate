plugins {
    id("kotlinMultiplatformBuildLogic") // from local composite build in ROOT/buildLogic/src/main/kotlin/BuildLogicGlobal.gradle.kts imported in ROOT/settings.gradle.kts
    id("io.kotest.multiplatform")
    kotlin("plugin.serialization")
    application
    //id("com.github.johnrengelman.shadow") // prefer using: gradle installDist && build/install/<appname>/bin/<appname>
}

group = "${rootProject.group}"
version = "${rootProject.version}"
val artifactName: String by extra { "${rootProject.name.lowercase()}-${project.name.lowercase()}" }

val rootPackage: String by rootProject.extra
val projectPackage: String by extra { "${rootPackage}.${project.name.lowercase()}" }
val theMainClass: String by extra { "Main" }
application {
    mainClass.set("${projectPackage}.${theMainClass}" + "Kt") // + "Kt" if fun main is outside a class
}

kotlin {
    val nativeTarget = when(BuildLogicGlobal.hostOS) {
        BuildLogicGlobal.HOSTOS.MACOS -> macosX64("native")
        BuildLogicGlobal.HOSTOS.LINUX -> linuxX64("native")
        BuildLogicGlobal.HOSTOS.WINDOWS -> mingwX64("native")
        else -> throw GradleException("Host OS is not supported in Kotlin/Native.")
    }

    nativeTarget.apply {
        binaries {
            executable {
                entryPoint = "${projectPackage}.${theMainClass.lowercase()}"
            }
        }
    }
    jvm {
        withJava()
    }
    sourceSets {
        val commonMain by getting  { // predefined by gradle multiplatform plugin
            dependencies {
                implementation("com.squareup.okio:okio:${libs.versions.okio.v()}")
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:${libs.versions.kotlinx.serialization.json.v()}")
                implementation("org.jetbrains.kotlinx:kotlinx-datetime:${libs.versions.kotlinx.datetime.v()}")
            }
        }
        val commonTest by getting {
            dependencies {
                implementation("io.kotest:kotest-framework-engine:${libs.versions.kotest.v()}")
                implementation("io.kotest:kotest-framework-datatest:${libs.versions.kotest.v()}")
                implementation("io.kotest:kotest-assertions-core:${libs.versions.kotest.v()}")
                implementation(kotlin("test-common"))
                implementation(kotlin("test-annotations-common"))
            }
        }
        val jvmMain by getting {
            dependencies {
                runtimeOnly("ch.qos.logback:logback-classic") { version { strictly(libs.versions.logback.v()) } }
            }
        }
        val jvmTest by getting {
            dependencies {
                //implementation(kotlin("test-junit"))
                //runtimeOnly("org.junit.jupiter:junit-jupiter-engine".depAndVersion())
                runtimeOnly("io.kotest:kotest-runner-junit5:${libs.versions.kotest.v()}") // depends on jvm { useJUnitPlatform() } // Platform!!! nd not only useJUnit()
            }
        }
        val nativeMain by getting
        val nativeTest by getting
    }
}

tasks {
    //shadowJar {
    //    manifest { attributes["Main-Class"] = theMainClass }
    //    archiveClassifier.set("fat")
    //    archiveBaseName.set(artifactName)
    //    // val jvmJar = named<org.gradle.jvm.tasks.Jar>("jvmJar").get()
    //    // from(jvmJar.archiveFile)
    //    // println(">   shadowJar adding fat.jar dependencies:")
    //    // project.configurations.named("jvmRuntimeClasspath").get().forEachIndexed { i, file ->
    //    //     println("     ${String.format("%2d", i+1)}) ${file.name}")
    //    // }
    //    // configurations.add(project.configurations.named("jvmRuntimeClasspath").get())
    //}
    //jar {
    //    finalizedBy(shadowJar)
    //}
}

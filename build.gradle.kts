import org.gradle.kotlin.dsl.provider.KotlinScriptBasePluginsApplicator
import org.jetbrains.kotlin.gradle.targets.jvm.KotlinJvmTarget

plugins {
    id("VersionsUpgradeBuildLogic")     // from local composite build in ROOT/buildLogic/src/main/kotlin/<id>.gradle.kts imported in ROOT/settings.gradle.kts
    id("kotlinMultiplatformBuildLogic") // from local composite build in ROOT/buildLogic/src/main/kotlin/<id>.gradle.kts imported in ROOT/settings.gradle.kts
    //kotlin("multiplatform")        version libs.versions.kotlin.asProvider().get()
    id("buildLogic.binaryPlugins.ProjectSetupBuildLogicPlugin")
    id("buildLogic.binaryPlugins.ProjectInfosBuildLogicPlugin")
    id("io.kotest.multiplatform")  version libs.versions.kotest.asProvider().get() apply false
    kotlin("plugin.serialization") version libs.versions.kotlin.asProvider().get() apply false
    //alias(libs.plugins.shadow) apply false // prefer using: gradle installDist && build/install/<appname>/bin/appname
    application
}

println("buildLogic information on ${project.name}${if (project.name == rootProject.name) " (ROOT project)" else " (subproject)"}:")
println("gradle --version = ${project.gradle.gradleVersion}")
BuildLogicGlobal.dump()

group = "com.hoffi"
version = "1.0-SNAPSHOT"
val artifactName: String by extra { "${rootProject.name.lowercase()}-${project.name.lowercase()}" }

val rootPackage: String by extra { "${rootProject.group}.${rootProject.name.replace("[-_]".toRegex(), "").lowercase()}" }
val projectPackage: String by extra { rootPackage }
val theMainClass: String by extra { "Main" }
application {
    mainClass.set("${rootPackage}.${theMainClass}" + "Kt") // + "Kt" if fun main is outside a class
}

projectSetupBuildLogic {
    basePackage.set(projectPackage)
}

allprojects {
    //logger.lifecycle("> root/build.gradle.kts allprojects: $project")
    afterEvaluate {
        logger.lifecycle("< Configure project ${project.name} evaluation finished.")
    }

    // some abbreviations and shortcuts
    fun Task.dependsOnIfExists(vararg name: String) = name.forEach { taskName: String ->
        try {
            dependsOn(project.tasks.named(taskName))
            logger.info("Task ${project.name}:${this.name} dependsOn(\"$taskName\")")
        } catch(_: Exception) { }
    }
    // 'c'ompile 'c'ommon (java in this case, but cc is just convenient to type)
    val cc by tasks.registering {
        val jvmCompileTasks     = arrayOf("compileKotlinJvm")
        val jvmCompileTestTasks = arrayOf("compileTestKotlinJvm")
        dependsOnIfExists(*(jvmCompileTasks + jvmCompileTestTasks))
    }
    // 'c'ompile 'n'ative
    val cn by tasks.registering {
        dependsOn(cc)
        val nativeCompileTasks     = arrayOf("compileKotlinNative",     "compileKotlinMacosX64",     "compileKotlinLinuxX64",     "compileKotlinMingwX64")
        val nativeCompileTestTasks = arrayOf("compileTestKotlinNative", "compileTestKotlinMacosX64", "compileTestKotlinLinuxX64", "compileTestKotlinMingwX64")
        dependsOnIfExists(*(nativeCompileTasks + nativeCompileTestTasks))
    }
    // versionsPrint
    afterEvaluate {// otherwise plugin might not have been applied yet
        val buildTask = tasks.findByName("build")
        val versionsPrintTask = tasks.findByName("versionsPrint")
        if (buildTask != null && versionsPrintTask != null) {
            buildTask.finalizedBy(versionsPrintTask)
        }
    }
}

subprojects {
    //logger.lifecycle("> root/build.gradle.kts subprojects for: sub$project")
    pluginManager.withPlugin("org.jetbrains.kotlin.jvm") {
        println("root/build.gradle.kts subprojects { configuring sub$project as kotlin(\"jvm\") project }")
    }
    pluginManager.withPlugin("org.jetbrains.kotlin.multiplatform") {
        println("root/build.gradle.kts subprojects { configuring sub$project as kotlin(\"multiplatform\") project }")
    }
}


kotlin {
    val nativeTarget = when(BuildLogicGlobal.hostOS) {
        BuildLogicGlobal.HOSTOS.MACOS -> macosX64("native")
        BuildLogicGlobal.HOSTOS.LINUX -> linuxX64("native")
        BuildLogicGlobal.HOSTOS.WINDOWS -> mingwX64("native")
        else -> throw GradleException("Host OS is not supported in Kotlin/Native.")
    }

    //jvmToolchain(BuildLogicGlobal.jdkVersion)

    nativeTarget.apply {
        binaries {
            executable {
                // entry point function = package with non-inside-object main method + ".main" (= name of the main function)
                entryPoint("${projectPackage}.main")
            }
        }
    }
    jvm {
        withJava()
    }
    sourceSets {
        val commonMain by getting { // predefined by gradle multiplatform plugin
            dependencies {
                implementation("com.squareup.okio:okio:${libs.versions.okio.v()}")
            }
        }
        val commonTest by getting {
            dependencies {
                //implementation("io.kotest:kotest-framework-engine:${libs.versions.kotest.asProvider().get()}")
                implementation("io.kotest:kotest-framework-engine:${libs.versions.kotest.v()}")
                implementation("io.kotest:kotest-framework-datatest:${libs.versions.kotest.v()}")
                implementation("io.kotest:kotest-assertions-core:${libs.versions.kotest.v()}")
                implementation(kotlin("test-common"))
                implementation(kotlin("test-annotations-common"))
            }
        }
        val jvmMain by getting {
            dependencies {
                runtimeOnly("ch.qos.logback:logback-classic") { version { strictly(libs.versions.logback.v()) } }
            }
        }
        val jvmTest by getting {
            dependencies {
                //implementation(kotlin("test-junit"))
                //runtimeOnly("org.junit.jupiter:junit-jupiter-engine:${libs.versions.junit.jupiter.v()}")
                runtimeOnly("io.kotest:kotest-runner-junit5:${libs.versions.kotest.v()}") // depends on jvm { useJUnitPlatform() } // Platform!!! nd not only useJUnit()
            }
        }
        val nativeMain by getting
        val nativeTest by getting
    }
}

//tasks {
//    withType(org.jetbrains.kotlin.gradle.targets.native.tasks.KotlinNativeTest::class) {
//        buildLogicCommonTestConfig("NATIVE")
//        // listen to standard out and standard error of the test JVM(s)
//        // onOutput { descriptor, event -> logger.lifecycle("Test: " + descriptor + " produced standard out/err: " + event.message ) }
//    }
//}

// ################################################################################################
// #####    pure informational stuff on stdout    #################################################
// ################################################################################################

// ==============================================================================
// ======   Helpers and pure informational stuff not necessary for build ========
// ==============================================================================

# kotlinMppTemplate README.md

This repo shows a standard kotlin multi project multiplatform layout</br>
using gradle composite builds for global `buildLogic/` across all subprojects like:

- version(numbers) of dependencies
- common plugin configuration (by applying convention plugins, which are basically XXX.gradle.kts files inside `buildLogic/src/main/kotlin/`)
- and some "general informational and helping" full blown binary-plugins each under `buildLogic/binaryPlugins/.../XXXBuildLogicPlugin/...`

some defined Global functions are usable in your build.gradle.kts and settings.gradle.kts files:

- buildLogic/src/main/kotlin/Helpers.kt
- buildLogic/src/main/kotlin/TestConfig.kt

atm there are the following convention plugins defined:

- `id("VersionsUpgradeBuildLogic")` // from `buildLogic/src/main/kotlin/<id>.gradle.kts`<br/>
    `gradle versionCatalogUPdate` your `libs.versions.toml` file (only dependencies that are actually in use by your project+subprojects)
- `id("kotlinMJvmBuildLogic")` // from `buildLogic/src/main/kotlin/<id>.gradle.kts`<br/>
  common setups for pure JVM gradle projects (like jvmToolchain version and configure test tasks)
- `id("kotlinMultiplatformBuildLogic")` // from `buildLogic/src/main/kotlin/<id>.gradle.kts`<br/>
    common setups of multiplatform gradle projects (like jvmToolchain version and configure test tasks)

atm there are the following binary plugins defined:

- id(`buildLogic.binaryPlugins.ProjectInfosBuildLogicPlugin`) from buildLogic/binaryPlugins/ProjectInfosBuildLogicPlugin<br/>
    report versions of kotlin, gradle, jvm, and some "key"-libraries
- id(`buildLogic.binaryPlugins.ProjectSetupBuildLogicPlugin`) from buildLogic/binaryPlugins/ProjectSetupBuildLogicPlugin<br/>
    create basepackages in any defined (root-folder does exist) srcSets<br/>
    and sentinel files in certain directories which can be used by Intellijs scopes and fileColors to get a better overview on deeply nested projects

## Troubleshooting

### upgrading kotlin and kotlin-native

#### Could not find :kotlin-native-prebuilt-windows-x86_64:1.9.10.

- try comment out `build.gradle.kts` `allprojects { }` and `subprojects { }` clauses
- create a plain vanilla intellij project choosing `template` and `multiplatform` in a folder outside your project(s) (via intellij: File -> New -> Project...)
  - build that one, if it succeeds, try clean rebuilding your/this project 

## Project Layout

### `ROOT/libs.versions.toml`

(For this template setup it should NOT(!) be in its default location inside the ROOT/gradle/ folder!)

contains the versions and (plugin)dependencies:

```toml
[versions]

[libraries]

[plugins]

[bundles]
```

(For this template setup it should NOT(!) be in its default location inside the ROOT/gradle/ folder!)

### root `settings.gradle`

Defines composite build(s) to be included at plugin resolution time (before(!) compilation starts).

```kotlin
pluginManagement {
    includeBuild("buildLogic")
    // binaryPlugins of buildLogic you want to use in your projects (as these do NOT propagate by just include("buildLogic")
    includeBuild("buildLogic/binaryPlugins/ProjectInfosBuildLogicPlugin")
    includeBuild("buildLogic/binaryPlugins/ProjectSetupBuildLogicPlugin")
    repositories {
        gradlePluginPortal()
        mavenCentral()
        google()
    }
}

dependencyResolutionManagement {
    ... repositories to use throughout all(sub) projects

      versionCatalogs {
          from(files("path/to/your/libs.versions.toml"))
      }
}
```

The subfolder `buildLogic/` contains a separate composite-build project(s) defining constants, (global)functions, tasks<br/>
convention-plugins (which are just `XXX.gradle.kts` files inside `includeBuild("YYYsubfolder)`'s `src/main/kotlin/` folder)
as well as where to search for external plugins.

`buildLogic/` has its own `settings.gradle.kts` as it is a composite-build

### structure of build.gradle.kts

in rootProject:

```kotlin
group = "com.hoffi"
version = "1.0-SNAPSHOT"
val artifactName: String by extra { "${rootProject.name.lowercase()}-${project.name.lowercase()}" }
val rootPackage: String by extra { "${rootProject.group}.${rootProject.name.lowercase()}" }
val projectPackage: String by extra { rootPackage }
val theMainClass: String by extra { "Main" }
application {
    mainClass.set("${rootPackage}.${theMainClass}" + "Kt") // + "Kt" if fun main is outside a class
}
```

in subprojects:

```kotlin
group = "${rootProject.group}"
version = "${rootProject.version}"
val artifactName: String by extra { "${rootProject.name.lowercase()}-${project.name.lowercase()}" }
val rootPackage: String by rootProject.extra
val projectPackage: String by extra { "${rootPackage}.${project.name.lowercase()}" }
val theMainClass: String by extra { "Main" }
application {
    mainClass.set("${projectPackage}.${theMainClass}" + "Kt") // + "Kt" if fun main is outside a class
}
```


```kotlin
plugins {
    kotlin("kotlinMJvmBuildLogic") version BuildSrcGlobal.VersionKotlin
}
dependencies {
    implementation("org.jetbrains.exposed:exposed-core".depAndVersion())
    implementation("org.jetbrains.exposed:exposed-kotlin-datetime".depButVersionOf("org.jetbrains.exposed:exposed-core"))
}
```

### Intellij idea scope coloring

in `ROOT/settings.gradle.kts`

pluginManagement {<br/>
&nbsp;&nbsp;&nbsp;&nbsp;includeBuild("buildLogic")<br/>
&nbsp;&nbsp;&nbsp;&nbsp;// special case of included builds are builds that define Gradle plugins.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;// These builds should be included using the includeBuild statement inside the pluginManagement {} block of the settings file.<br/>
&nbsp;&nbsp;&nbsp;&nbsp;// Using this mechanism, the included build may also contribute a settings plugin that can be applied in the settings file itself.<br/>
`    includeBuild("buildLogic/binaryPlugins/ProjectInfosBuildLogicPlugin")`<br/>
`    includeBuild("buildLogic/binaryPlugins/ProjectSetupBuildLogicPlugin")`<br/>
&nbsp;&nbsp;&nbsp;&nbsp;repositories {<br/>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;...<br/>
    }<br/>
}<br/>


for better orientation while code browsing you can use intellij scopes to color certain files/folders/modules.

these are defined in `.idea/scopes`

and the colors taken for each scope in `.idea/fileColors.xml`

make sure you do not .gitignore these, by e.g.:

```gitignore
.idea/
!.idea/scopes/
!.idea/fileColors.xml
```

and/or define your own scopes (in Settings... `Scopes`):

- scope `00__` (scope with all folders where the name starts with: `0[0-3]__`, meaning the first folder
  - `file:01__*/||file:02__*/||file:03__*/||file:ZZ__*/||file:ZZ__*`
- scope `src`
  - `file:_src*/` (scope with all folders where the name starts with `_src`)
- scope `buildfiles`
  - `file:build.gradle.kts||file:gradle.properties||file:settings.gradle.kts`

and then in Settings... `File Colors` add the scope(s) and give them a color.

If you _then_ add folders/files matching the above scope names
you can see more clearly which "area" of code in the folder structure you are just looking at the moment.

a gradle helper task that you can copy into root's build.gradle.kts can be found in `buildSrc/snippets/`

package com.hoffi.kotlinmpptemplate.apimodel

fun main() {
    println("==================================================")
    println("===   Hello, apiModel Kotlin/Native! (${Platform.osFamily} / ${Platform.cpuArchitecture})   ===")
    println("==================================================")
}

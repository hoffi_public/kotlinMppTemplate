package com.hoffi.kotlinmpptemplate.apimodel

fun main() {
    println("==============================================================")
    println("===   Hello, apiModel Kotlin/JVM!  (${System.getProperty("os.name")} / ${System.getProperty("os.arch")})   ===")
    println("==============================================================")
}

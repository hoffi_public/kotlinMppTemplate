plugins {
    //kotlin("multiplatform") // if not using buildLogic convention plugin below: id("kotlinMultiplatformBuildLogic")
    id("kotlinMultiplatformBuildLogic") // from local composite build in ROOT/buildLogic/src/main/kotlin/BuildLogicGlobal.gradle.kts imported in ROOT/settings.gradle.kts
    id("io.kotest.multiplatform")
    kotlin("plugin.serialization")
    //id("com.github.johnrengelman.shadow") // prefer using: gradle installDist && build/install/<appname>/bin/<appname>
    application
    id("buildLogic.binaryPlugins.ProjectInfosBuildLogicPlugin")
}

group = "${rootProject.group}"
version = "${rootProject.version}"
val artifactName: String by extra { "${rootProject.name.lowercase()}-${project.name.lowercase()}" }

val rootPackage: String by rootProject.extra
val projectPackage: String by extra { "${rootPackage}.${project.name.lowercase()}" }
val theMainClass: String by extra { "Main" }
application {
    mainClass.set("${projectPackage}.${theMainClass}" + "Kt") // + "Kt" if fun main is outside a class
}

// as io.insert-koin:koin-test imported older kotlin version test stuff
configurations.all {
    resolutionStrategy.capabilitiesResolution.withCapability("org.jetbrains.kotlin:kotlin-test-framework-impl") {
        selectHighestVersion()
    }
}

kotlin {
    val nativeTarget = when(BuildLogicGlobal.hostOS) {
        BuildLogicGlobal.HOSTOS.MACOS -> macosX64("native")
        BuildLogicGlobal.HOSTOS.LINUX -> linuxX64("native")
        BuildLogicGlobal.HOSTOS.WINDOWS -> mingwX64("native")
        else -> throw GradleException("Host OS is not supported in Kotlin/Native.")
    }

    nativeTarget.apply {
        binaries {
            executable {
                entryPoint = "${projectPackage}.${theMainClass.lowercase()}"
            }
        }
    }
    jvm {
        //jvmToolchain(BuildLogicGlobal.jdkVersion)
        //withJava()
        //testRuns.named("test") {
        //    executionTask.configure {
        //        useJUnitPlatform()
        //    }
        //}
    }
    sourceSets {
        val commonMain by getting  { // predefined by gradle multiplatform plugin
            dependencies {
                //implementation(kotlin("reflect"))
                implementation(libs.okio)
                implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:${libs.versions.kotlinx.coroutines.v()}")
                implementation("org.jetbrains.kotlinx:kotlinx-datetime:${libs.versions.kotlinx.datetime.v()}")
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-json:${libs.versions.kotlinx.serialization.json.v()}")
                implementation("io.insert-koin:koin-core:${libs.versions.koin.v()}")
                implementation("co.touchlab:kermit:${libs.versions.kermit.v()}")
                implementation("co.touchlab:kermit-koin:${libs.versions.kermit.v()}")
            }
        }
        val commonTest by getting {
            dependencies {
                // tests junit5
                implementation(kotlin("test-common"))
                implementation(kotlin("test-annotations-common"))
                // tests kotest
                implementation("io.kotest:kotest-framework-engine:${libs.versions.kotest.v()}")
                implementation("io.kotest:kotest-framework-datatest:${libs.versions.kotest.v()}")
                implementation("io.kotest:kotest-assertions-core:${libs.versions.kotest.v()}")

                implementation("io.kotest.extensions:kotest-extensions-koin:${libs.versions.kotest.extensions.koin.v()}")
                implementation("io.insert-koin:koin-test:${libs.versions.kointest.v()}")
            }
        }
        val jvmMain by getting {
            dependencies {
                runtimeOnly("ch.qos.logback:logback-classic") { version { strictly(libs.versions.logback.v()) } }
            }
        }
        val jvmTest by getting {
            dependencies {
                // for running junit tests
                implementation(kotlin("test"))
                // for running kotest tests
                implementation("io.kotest:kotest-runner-junit5-jvm:${libs.versions.kotest.v()}")
            }
        }
        val nativeMain by getting
        val nativeTest by getting
    }
}

tasks {
    //withType<Test> {
    //    systemProperty("kotest.framework.dump.config", "true")
    //}

    //shadowJar {
    //    manifest { attributes["Main-Class"] = theMainClass }
    //    archiveClassifier.set("fat")
    //    archiveBaseName.set(artifactName)
    //    // val jvmJar = named<org.gradle.jvm.tasks.Jar>("jvmJar").get()
    //    // from(jvmJar.archiveFile)
    //    // println(">   shadowJar adding fat.jar dependencies:")
    //    // project.configurations.named("jvmRuntimeClasspath").get().forEachIndexed { i, file ->
    //    //     println("     ${String.format("%2d", i+1)}) ${file.name}")
    //    // }
    //    // configurations.add(project.configurations.named("jvmRuntimeClasspath").get())
    //}
    //jar {
    //    finalizedBy(shadowJar)
    //}
}

projectInfosBuildLogic {
    onlyJarVersionNamesByRegex.set(true)
}

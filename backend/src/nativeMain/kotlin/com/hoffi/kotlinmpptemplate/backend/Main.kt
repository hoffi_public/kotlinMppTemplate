package com.hoffi.kotlinmpptemplate.backend

fun main() {
    println("==================================================")
    println("===   Hello, backend Kotlin/Native! (${Platform.osFamily} / ${Platform.cpuArchitecture})   ===")
    println("==================================================")
    mainCommon(emptyArray())
}

package com.hoffi.kotlinmpptemplate.backend

actual class Dummy actual constructor(dummyDep: DummyDep): IDummy {
    actual val dummyDep = dummyDep
    actual override val p: String = "NATIVE"
    actual override fun f(): String {
        return "$p with $dummyDep"
    }
}

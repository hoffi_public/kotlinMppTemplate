package com.hoffi.kotlinmpptemplate.backend

import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import org.koin.core.context.startKoin
import org.koin.core.module.dsl.bind
import org.koin.core.module.dsl.singleOf
import org.koin.core.parameter.parametersOf
import org.koin.dsl.module

fun mainCommon(@Suppress("UNUSED_PARAMETER") args: Array<String>) {
    val koinApp = startKoin {
        //logger(PrintLogger)
        modules(
            appModule
        )
    }
    val doIt: DoIt = koinApp.koin.get(DoIt::class, parameters =  { parametersOf(Par("explicit")) })
    doIt.doIt()
}

val appModule = module {
    factory { params -> DoIt(get(), params.get()) }
    singleOf(::Dummy) { bind<IDummy>() }
    singleOf(::DummyDep)
}

data class Par(val par: String)

class DoIt(val dummy: Dummy, val par: Par) : KoinComponent {
    private val otherDummy: Dummy by inject()
    fun doIt() {
        println("par='${par}' ${dummy.f()}")
        println("par='${par}' ${otherDummy.f()}")
    }
}

package com.hoffi.kotlinmpptemplate.backend

import kotlin.jvm.JvmStatic

interface IDummy {
    val p: String
    fun f(): String
}
expect class Dummy(dummyDep: DummyDep): IDummy {
    val dummyDep: DummyDep
    override val p: String
    override fun f(): String
}

class DummyDep {
    override fun toString() = "DummyDep(dp='$dp', ${f()})"
    val dp: String = "depDummy"
    fun f() = "$dp(${count++})"

    companion object {
        var count = 1L
    }
}

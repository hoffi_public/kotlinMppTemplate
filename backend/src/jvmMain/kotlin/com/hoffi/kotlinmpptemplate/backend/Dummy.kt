package com.hoffi.kotlinmpptemplate.backend

actual class Dummy actual constructor(actual val dummyDep: DummyDep): IDummy {
    //actual val dummyDep = dummyDep
    actual override val p: String = "JVM"
    actual override fun f(): String {
        return "$p with $dummyDep"
    }
}

package com.hoffi.kotlinmpptemplate.backend

fun main() {
    println("==============================================================")
    println("===   Hello, backend Kotlin/JVM!  (${System.getProperty("os.name")} / ${System.getProperty("os.arch")})   ===")
    println("==============================================================")
    mainCommon(emptyArray())
}

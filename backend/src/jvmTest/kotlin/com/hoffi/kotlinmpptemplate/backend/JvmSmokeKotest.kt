package com.hoffi.kotlinmpptemplate.backend

import io.kotest.core.spec.style.ShouldSpec
import io.kotest.matchers.shouldBe
import org.koin.test.KoinTest

class JvmSmokeKotest : ShouldSpec(), KoinTest {
    init {
        should("return the length of the string").config(invocations = 10, threads = 2) {
            "sammy".length shouldBe  5
            "".length shouldBe 0
        }
    }
}

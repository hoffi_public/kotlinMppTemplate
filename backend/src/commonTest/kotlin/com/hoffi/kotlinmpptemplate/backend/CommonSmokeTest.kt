package com.hoffi.kotlinmpptemplate.backend

import kotlin.test.Test
import kotlin.test.assertEquals

class CommonSmokeTest {
    @Test
    fun testSum() {
        val expected = 42
        assertEquals(expected, 42)
    }
}

package com.hoffi.kotlinmpptemplate.backend

import io.kotest.common.ExperimentalKotest
import io.kotest.engine.test.logging.info
import io.kotest.matchers.string.shouldEndWith
import io.kotest.matchers.string.shouldNotEndWith
//import org.koin.core.component.inject
import org.koin.test.inject

@OptIn(ExperimentalKotest::class)
class MainTest : KoinBddSpec(appModule, behaviorSpec = {
    Given("an injected Dummy") {
        val dummy: Dummy by inject()
        When("calling f() on injected Dummy") {
            info { "${MainTest::class.simpleName}: When(1)"}
            val result = dummy.f()
            Then("result should be 1") {
                result shouldEndWith "with DummyDep(dp='depDummy', depDummy(1))"
            }
            Then("result should not be 2") {
                result shouldNotEndWith  "with DummyDep(dp='depDummy', depDummy(2))"
            }
        }
        When("calling f() again on injected Dummy") {
            info { "${MainTest::class.simpleName}: When(2)"}
            val result = dummy.f()
            Then("result should be 2") {
                result shouldEndWith "with DummyDep(dp='depDummy', depDummy(2))"
            }
        }
    }
})

package com.hoffi.kotlinmpptemplate.backend

//import io.kotest.core.annotation.EnabledCondition
//import io.kotest.core.annotation.EnabledIf
//import io.kotest.core.spec.Spec
import io.kotest.core.spec.style.BehaviorSpec
import io.kotest.koin.KoinExtension
import io.kotest.koin.KoinLifecycleMode
import org.koin.core.module.Module
import org.koin.test.KoinTest

//class KotestKoinBddSpecBaseClassCondition : EnabledCondition {
//    override fun enabled(kclass: KClass<out Spec>): Boolean = !(kclass.simpleName?.equals("KoinBddSpec") ?: false)
//}
//@EnabledIf(KotestKoinBddSpecBaseClassCondition::class)
abstract class KoinBddSpec(val koinModules: List<Module>, behaviorSpec: KoinBddSpec.() -> Unit): KoinTest, BehaviorSpec(behaviorSpec as BehaviorSpec.() -> Unit) {
    constructor(vararg koinModules: Module, behaviorSpec: KoinBddSpec.() -> Unit) : this(koinModules.asList(), behaviorSpec)
    override fun extensions() = koinModules.map { KoinExtension(module = it, mockProvider = null, mode = KoinLifecycleMode.Root) }
}
